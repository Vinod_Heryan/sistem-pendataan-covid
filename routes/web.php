<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout.manual');

Auth::routes();

// Route::get('admin/home', [App\Http\Controllers\HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('dashboard/admin', [App\Http\Controllers\DashboardAdminController::class, 'index'])->name('dashboard.admin');

Route::get('search/desa', [App\Http\Controllers\DesaController::class, 'search'])->name('desa.search');
Route::get('search/admin', [App\Http\Controllers\RegisterManualController::class, 'search'])->name('admin.search');
Route::get('/search/detail/pasien', [App\Http\Controllers\DetailController::class, 'searchPasien'])->name('detail.search.pasien');
Route::get('/search/detail/vaksin', [App\Http\Controllers\DetailController::class, 'searchDetailVaksin'])->name('detail.search.vaksin');
Route::get('/search/detail/vaksin/{nama_vaksin}', [App\Http\Controllers\DetailController::class, 'searchVaksin'])->name('detail.search.ke.vaksin');
Route::get('/search/detail/positif', [App\Http\Controllers\DetailController::class, 'searchPositif'])->name('detail.search.positif');
Route::get('/search/detail/sembuh', [App\Http\Controllers\DetailController::class, 'searchSembuh'])->name('detail.search.sembuh');
Route::get('/search/detail/inap', [App\Http\Controllers\DetailController::class, 'searchInap'])->name('detail.search.inap');
Route::get('/searchkondisi/detail/positif/{kondisi}', [App\Http\Controllers\DetailController::class, 'searchkondisi'])->name('detail.search.kondisi');
Route::get('/searchkondisi/detail/inap/{kondisi}', [App\Http\Controllers\DetailController::class, 'searchinapkondisi'])->name('detail.search.inap.kondisi');
Route::get('/search/detail/penduduk/{nama_desa}', [App\Http\Controllers\DetailController::class, 'searchdesa'])->name('detail.penduduk.search.desa');

Route::get('add/desa', [App\Http\Controllers\DesaController::class, 'add'])->name('add.desa');
Route::get('add/user', [App\Http\Controllers\RegisterManualController::class, 'halamanregister'])->name('add.user');

Route::get('desa', [App\Http\Controllers\DesaController::class, 'index'])->name('desa');
Route::get('admin', [App\Http\Controllers\RegisterManualController::class, 'index'])->name('admin');
Route::get('/detail/pasien', [App\Http\Controllers\DetailController::class, 'pasien'])->name('detail.pasien');
Route::get('/detail/daftar/pasien/{id}', [App\Http\Controllers\DetailController::class, 'detailPasien'])->name('detail.daftar.pasien');
Route::get('/detail/vaksin', [App\Http\Controllers\DetailController::class, 'vaksin'])->name('detail.vaksin');
Route::get('/detail/positif', [App\Http\Controllers\DetailController::class, 'positif'])->name('detail.positif');
Route::get('/detail/sembuh', [App\Http\Controllers\DetailController::class, 'sembuh'])->name('detail.sembuh');
Route::get('/detail/inap', [App\Http\Controllers\DetailController::class, 'inap'])->name('detail.inap');

Route::get('/update/desa/{id}', [App\Http\Controllers\DesaController::class, 'ubah'])->name('hal.update.desa');
Route::get('/update/admin/{id}', [App\Http\Controllers\RegisterManualController::class, 'ubah'])->name('hal.update.admin');

Route::post('put/desa', [App\Http\Controllers\DesaController::class, 'update'])->name('update.desa');
Route::post('put/admin', [App\Http\Controllers\RegisterManualController::class, 'update'])->name('update.admin');

Route::get('/delete/desa/{id}', [App\Http\Controllers\DesaController::class, 'hapus']);
Route::get('/delete/admin/{id}', [App\Http\Controllers\RegisterManualController::class, 'hapus']);

Route::post('store/desa', [App\Http\Controllers\DesaController::class, 'store'])->name('store.desa');
Route::post('store/user', [App\Http\Controllers\RegisterManualController::class, 'register'])->name('store.user');


//============================================== Admin Desa===========================================================

Route::get('penduduk', [App\Http\Controllers\AdminDesa\PendudukController::class, 'index'])->name('penduduk');
Route::get('positif', [App\Http\Controllers\AdminDesa\PositifController::class, 'index'])->name('positif');
Route::get('sembuh', [App\Http\Controllers\AdminDesa\SembuhController::class, 'index'])->name('sembuh');
Route::get('vaksin', [App\Http\Controllers\AdminDesa\VaksinController::class, 'index'])->name('vaksin');
Route::get('inap', [App\Http\Controllers\AdminDesa\InapController::class, 'index'])->name('inap');
Route::get('dashboard/desa/{id}', [App\Http\Controllers\AdminDesa\DashboardDesaController::class, 'index'])->name('dashboard.desa');
Route::get('/dashboard/desa/db', [App\Http\Controllers\AdminDesa\DashboardDesaController::class, 'db'])->name('db');


Route::get('add/penduduk', [App\Http\Controllers\AdminDesa\PendudukController::class, 'add'])->name('add.penduduk');
Route::get('add/positif', [App\Http\Controllers\AdminDesa\PositifController::class, 'add'])->name('add.positif');
Route::get('add/vaksin', [App\Http\Controllers\AdminDesa\VaksinController::class, 'add'])->name('add.vaksin');
Route::get('add/inap', [App\Http\Controllers\AdminDesa\InapController::class, 'add'])->name('add.inap');
Route::get('cekname/positif', [App\Http\Controllers\AdminDesa\PositifController::class, 'cekname'])->name('cekname');
Route::get('cekname/vaksin', [App\Http\Controllers\AdminDesa\VaksinController::class, 'cekname'])->name('cekname.vaksin');
Route::get('cekname/inap', [App\Http\Controllers\AdminDesa\InapController::class, 'cekname'])->name('cekname.inap');

Route::post('store/penduduk', [App\Http\Controllers\AdminDesa\PendudukController::class, 'store'])->name('store.penduduk');
Route::post('store/positif', [App\Http\Controllers\AdminDesa\PositifController::class, 'store'])->name('store.positif');
Route::post('store/vaksin', [App\Http\Controllers\AdminDesa\VaksinController::class, 'store'])->name('store.vaksin');
Route::post('store/inap', [App\Http\Controllers\AdminDesa\InapController::class, 'store'])->name('store.inap');

Route::get('/update/penduduk/{id}', [App\Http\Controllers\AdminDesa\PendudukController::class, 'ubah'])->name('hal.update.penduduk');
Route::get('/update/positif/sembuh/{id}', [App\Http\Controllers\AdminDesa\PositifController::class, 'updateSembuh']);
Route::get('/update/positif/meninggal/{id}', [App\Http\Controllers\AdminDesa\PositifController::class, 'updateMeninggal']);
Route::get('/update/inap/{id}', [App\Http\Controllers\AdminDesa\InapController::class, 'update']);

Route::get('/detail/penduduk/{id}', [App\Http\Controllers\AdminDesa\PendudukController::class, 'detail'])->name('penduduk.detail');

Route::get('/delete/penduduk/{id}', [App\Http\Controllers\AdminDesa\PendudukController::class, 'hapus']);
Route::get('/delete/positif/{id}', [App\Http\Controllers\AdminDesa\PositifController::class, 'hapus']);
Route::get('/delete/sembuh/{id}', [App\Http\Controllers\AdminDesa\SembuhController::class, 'hapus']);
Route::get('/delete/vaksin/{id}', [App\Http\Controllers\AdminDesa\VaksinController::class, 'hapus']);
Route::get('/delete/inap/{id}', [App\Http\Controllers\AdminDesa\InapController::class, 'hapus']);

Route::post('put/penduduk', [App\Http\Controllers\AdminDesa\PendudukController::class, 'update'])->name('update.penduduk');

Route::get('search/penduduk', [App\Http\Controllers\AdminDesa\PendudukController::class, 'search'])->name('penduduk.search');
Route::get('/search/penduduk/{nama_desa}', [App\Http\Controllers\AdminDesa\PendudukController::class, 'searchdesa'])->name('penduduk.search.desa');
Route::get('search/positif', [App\Http\Controllers\AdminDesa\PositifController::class, 'search'])->name('positif.search');
Route::get('search/sembuh', [App\Http\Controllers\AdminDesa\SembuhController::class, 'search'])->name('sembuh.search');
Route::get('search/vaksin', [App\Http\Controllers\AdminDesa\VaksinController::class, 'search'])->name('vaksin.search');
Route::get('/searchvaksin/vaksin/{nama_vaksin}', [App\Http\Controllers\AdminDesa\VaksinController::class, 'searchvaksin'])->name('vaksin.ke.search');
Route::get('search/inap', [App\Http\Controllers\AdminDesa\InapController::class, 'search'])->name('inap.search');
Route::get('/searchkondisi/positif/{kondisi}', [App\Http\Controllers\AdminDesa\PositifController::class, 'searchkondisi'])->name('kondisi.search');
Route::get('/searchkondisi/inap/{kondisi}', [App\Http\Controllers\AdminDesa\InapController::class, 'searchkondisi'])->name('kondisi.inap.search');

Route::get('search/pdf/positif', [App\Http\Controllers\AdminDesa\PDFDesaController::class, 'searchpositif'])->name('pdf.positif.search');
Route::get('search/pdf/inap', [App\Http\Controllers\AdminDesa\PDFDesaController::class, 'searchinap'])->name('pdf.inap.search');
Route::get('search/pdf/vaksin', [App\Http\Controllers\AdminDesa\PDFDesaController::class, 'searchvaksin'])->name('pdf.vaksin.search');

Route::get('/pdf/positif', [App\Http\Controllers\AdminDesa\PDFDesaController::class, 'positif'])->name('pdf.positif');
Route::get('/pdf/inap', [App\Http\Controllers\AdminDesa\PDFDesaController::class, 'inap'])->name('pdf.inap');
Route::get('/pdf/vaksin', [App\Http\Controllers\AdminDesa\PDFDesaController::class, 'vaksin'])->name('pdf.vaksin');

Route::get('/download/positif/{id}', [App\Http\Controllers\AdminDesa\PDFDesaController::class, 'downloadpositif']);
Route::get('/download/inap/{id}', [App\Http\Controllers\AdminDesa\PDFDesaController::class, 'downloadinap']);
Route::get('/download/vaksin/{id}', [App\Http\Controllers\AdminDesa\PDFDesaController::class, 'downloadvaksin']);


//============================================== User ===========================================================

Route::get('/', [App\Http\Controllers\UserPage\DashboardController::class, 'index'])->name('dashboard');

Route::get('data/lokasi', [App\Http\Controllers\UserPage\DataLokasiController::class, 'index'])->name('data.lokasi');

Route::get('search/data/lokasi', [App\Http\Controllers\UserPage\DataLokasiController::class, 'search'])->name('data.lokasi.search');

Route::get('/data/lokasi/laporan/{id}', [App\Http\Controllers\UserPage\DataLokasiController::class, 'laporan'])->name('data.laporan');

 


