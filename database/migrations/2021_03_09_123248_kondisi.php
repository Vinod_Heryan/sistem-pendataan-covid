<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Kondisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sembuh', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('penduduk_id');
            $table->foreign('penduduk_id')->references('id')->on('penduduk');
            $table->date('tanggal');
            $table->timestamps();
        });

        Schema::create('positif', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('penduduk_id');
            $table->foreign('penduduk_id')->references('id')->on('penduduk');
            $table->date('tanggal');
            $table->timestamps();
        });

        Schema::create('meninggal', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('penduduk_id');
            $table->foreign('penduduk_id')->references('id')->on('penduduk');
            $table->date('tanggal');
            $table->timestamps();
        });

        Schema::create('vaksin', function (Blueprint $table) {
            $table->id();
            $table->string('nama_vaksin');
            $table->unsignedBigInteger('penduduk_id');
            $table->foreign('penduduk_id')->references('id')->on('penduduk');
            $table->date('tanggal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sembuh');
        Schema::dropIfExists('positif');
        Schema::dropIfExists('meninggal');
        Schema::dropIfExists('vaksin');
    }
}
