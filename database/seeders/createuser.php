<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class createuser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'Admin',
                'desa_id' => '1',
                'email' => 'admin@gmail.com',
                'is_admin' => '1',
                'password' => bcrypt('123456'),
            ],

            [
                'name' => 'Desa',
                'desa_id' => '1',
                'email' => 'desa@gmail.com',
                'is_admin' => '0',
                'password' => bcrypt('123456'),
            ],
        ];
        foreach($user as $key => $value){
            User::create($value);
        }
    }
}
