<?php

namespace Database\Seeders;

use App\Models\Desa;
use Illuminate\Database\Seeder;

class createdesa extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $desa = [
            [
                'name' => 'Desa Serimbu',
                'alamat' => 'Kabupaten Landak, Kalimantan Barat',
            ],
        ];
        foreach($desa as $key => $value){
            Desa::create($value);
    }
}
}
