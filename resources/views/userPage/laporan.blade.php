@extends('layouts.app')
@section('content')

@if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif

<div class="card-header">
@foreach($data as $d)
<h1>Laporan Data {{$d->name}}</h1>
@endforeach
</div> 
<div class="card-body">
<div class="row">
<span class="card text-white bg-primary mb-3" style="width: 30%;">
  <h3 class="card-header">Penduduk</h3>
  <div class="card-body">
  <img src="{{asset('tes/dist/img/1.png')}}" width="50px" height="50px" alt="AdminLTE Logo" class="brand-image img-circle elevation-3">
    <h3 class="text-right">{{number_format($penduduk)}} org</h3>
  </div>
</span>

<span class="card text-white bg-danger mb-3" style="width: 30%; margin: 0% 5% 0% 5%;">
  <h3 class="card-header">Kasus Positif</h3>
  <div class="card-body">
  <img src="{{asset('tes/dist/img/2.png')}}" width="50px" height="50px" alt="AdminLTE Logo"  class="brand-image img-circle elevation-3">
    <h3 class="text-right">{{number_format($positif)}} kasus</h3>
  </div>
</span>

<span class="card text-white bg-info mb-3" style="width: 30%;">
  <h3 class="card-header">Sembuh</h3>
  <div class="card-body">
  <img src="{{asset('tes/dist/img/3.png')}}" width="50px" height="50px" alt="AdminLTE Logo" class="brand-image img-circle elevation-3">
    <h3 class="text-right">{{number_format($sembuh)}} org</h3>
  </div>
</span>

<span class="card text-white bg-success mb-3" style="width: 30%;">
  <h3 class="card-header">Vaksin</h3>
  <div class="card-body">
  <img src="{{asset('tes/dist/img/4.png')}}" width="50px" height="50px" alt="AdminLTE Logo" class="brand-image img-circle elevation-3">
    <h3 class="text-right">{{number_format($vaksin)}} org</h3>
  </div>
</span>

<span class="card text-white bg-warning mb-3" style="width: 30%; margin: 0% 5% 0% 5%;">
  <h3 class="card-header">Rawat Inap</h3>
  <div class="card-body">
  <img src="{{asset('tes/dist/img/5.png')}}" width="50px" height="50px" alt="AdminLTE Logo" class="brand-image img-circle elevation-3">
    <h3 class="text-right">{{number_format($inap)}} org</h3>
  </div>
</span>

<span class="card text-white bg-secondary mb-3" style="width: 30%;">
  <h3 class="card-header">Meninggal</h3>
  <div class="card-body">
  <img src="{{asset('tes/dist/img/8.png')}}" width="50px" height="50px" alt="AdminLTE Logo" class="brand-image img-circle elevation-3">
    <h3 class="text-right">{{number_format($meninggal)}} org</h3>
  </div>
</span>
</div>
</div>
@endsection