@extends('layouts.admin')
@section('content')
    @if ($message = Session::get('success'))
	  <div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
		  <strong>{{ $message }}</strong>
	  </div>
	@endif

	@if ($message = Session::get('update'))
	  <div class="alert alert-warning alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif

    @if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif
  
<div class="card">
<div class="card-body">
<nav class="navbar navbar-light bg-light">
<h2>Daftar Desa</h2>
  <form class="form-inline" action="{{route('desa.search')}}" method="get">
    <input class="form-control mr-sm-2" type="search" placeholder="Search Kode dan Nama" name="search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
</nav>
<div class="table-responsive">
<table class="table table-striped" width="100%">
<thead>
    <tr>
        <th width="10%">NO</td>
        <th width="10%">Kode Desa</th>
        <th width="25%">Nama Desa</th>
        <th width="35%">Lokasi Desa</th>
        <th width="20%">Action</th>
    </tr>
</thead>
    <?php
    $no=1;
    ?>
    @foreach($table as $t)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$t->id}}</td>
        <td>{{$t->name}}</td>
        <td>{{$t->alamat}}</td>
        <td><a href="/update/desa/{{$t->id}}" class="btn btn-outline-warning my-2 my-sm-0">Ubah</a> | | <a href="/delete/desa/{{$t->id}}" class="btn btn-outline-danger my-2 my-sm-0">hapus</a></td>
    </tr>
    @endforeach
</table>
</div>
<div class="font-italic">
Halaman Ke 
{{$table->currentPage()}}
Dari
{{$table->lastPage()}}
Halaman
</div>
<div>
{{$table->links()}}
</div>
</div>
</div>
@endsection


