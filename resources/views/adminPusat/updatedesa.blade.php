@extends('layouts.admin')
@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
             @foreach ($errors->all() as $error)
             <button type="button" class="close" data-dismiss="alert">×</button>	
              <li>{{ $error }}</li>
             @endforeach
    </div>
@endif
<div class="card-header">
    <h2>Update Data Desa</h2>
</div>
<div class="card-body">
<table>
<form action="{{route('update.desa')}}" id="usrform" method="post">
@foreach($table as $t)
{{csrf_field()}}
    <div class="form-group">
        <input type="hidden" name="id" class="form-control" placeholder="Masukan Kode Desa Baru" value="{{$t->id}}">
    </div>
    <div class="form-group">
        <label>Nama Desa</label>
        <input type="text" name="name" value="{{$t->name}}" class="form-control" placeholder="Masukan Nama Desa Baru" required="required">
    </div>
    <div class="form-group">
        <label>Lokasi Desa</label>
        <textarea rows="2" cols="50" name="alamat" form="usrform" class="form-control" placeholder="Masukan Lokasi Baru Desa" required="required">{{$t->alamat}}</textarea>
    </div>
        <input type="submit" class="btn btn-primary" value="Simpan Data">
@endforeach
</form> 
</table>
</div>
@endsection