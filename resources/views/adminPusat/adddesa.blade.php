@extends('layouts.admin')
@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
             @foreach ($errors->all() as $error)
             <button type="button" class="close" data-dismiss="alert">×</button>	
              <li>{{ $error }}</li>
             @endforeach
    </div>
@endif
<div class="card-header">
    <h2>Tambah Data Desa</h2>
</div>
<div class="card-body">
<table>
<form action="{{route('store.desa')}}" id="usrform" method="post">
{{csrf_field()}}
    <div class="form-group">
        <label for="kodedesa">Kode Desa</label>
        <input type="text" name="id" class="form-control" id="kodedesa" placeholder="Masukan Kode Desa" required="required">
    </div>
    <div class="form-group">
        <label for="namadesa">Nama Desa</label>
        <input type="text" name="name" class="form-control" id="namadesa" placeholder="Masukan Nama Desa" required="required">
        <small class="form-text text-muted">Jika Inputan Terlalu Panjang Disarankan Untuk Disingkat</small>
    </div>
    <div class="form-group">
        <label for="lokasidesa" >Lokasi Desa</label>
        <textarea rows="3" cols="50" name="alamat" form="usrform" id="lokasidesa" class="form-control" placeholder="Masukan Lokasi Desa" required="required"></textarea>
    </div>
        <input type="submit" class="btn btn-primary" value="Simpan Data">
</form> 
</table>
</div>

@endsection