@extends('layouts.admin')
@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
             @foreach ($errors->all() as $error)
             <button type="button" class="close" data-dismiss="alert">×</button>	
              <li>{{ $error }}</li>
             @endforeach
    </div>
@endif

<div class="card-header">
    <h2>Register Admin Desa</h2>
</div>
<div class="card-body">
<table>
<form action="{{route('store.user')}}" method="post">
{{csrf_field()}}
    <div class="form-group">
        <label for="namaadmin">Nama Admin</label>
        <input type="text" name="name" class="form-control" id="namaadmin" placeholder="Masukan Nama Admin" required="required">
    </div>
    <div class="form-group">
        <label for="namaadmin">Kategori Admin</label>
        <select name="is_admin" class="custom-select" required="required">
            <option value="" selected>--Pilih Kategori--</option>
            <option value="1">Admin Pusat</option>
            <option value="0">Admin Desa</option>
            </select>
    </div>
    <div class="form-group">
        <label for="namaadmin">Lokasi</label>
        <select name="desa_id" class="custom-select" required="required">
            <option value="" selected>--Pilih Lokasi--</option>
            @foreach($data as $d)
            <option value="{{$d->id}}">{{$d->name}}</option>
            @endforeach
            </select>
    </div>
    <div class="form-group">
        <label for="emailadmin">Email</label>
        <input type="email" name="email" class="form-control" placeholder="Masukan Email" required="required">
        <small class="form-text text-muted">Harus Menggunakan Email dan Belum Pernah Terdaftar</small>
    </div>
    <div class="form-group">
        <label for="passwordadmin">Password</label>
        <input type="password" name="password" class="form-control" placeholder="Masukan Password" required="required">
        <small class="form-text text-muted"> Minimal 8 Karakter</small>
    </div>
        <input type="submit" class="btn btn-primary" value="Simpan Data">
</form> 
</table>
</div>
@endsection