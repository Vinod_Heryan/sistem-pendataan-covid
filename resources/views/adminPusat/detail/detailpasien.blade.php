@extends('layouts.admin')
@section('content')
<div class="card">
<div class="card-body" >
<h2>Detail Data Penduduk</h2>
<div class="table-responsive">
<table class="table table-striped" width="100%" >
<thead>
    <tr>
        <th width="5%">No</td>
        <th width="5%">Kode</td>
        <th width="15%">NIK</th>
        <th width="15%">Nama</th>
        <th width="20%">No Telpon</th>
        <th width="15%">Jenis Kelamin</th>
        <th width="25%">Alamat</th>
    </tr>
</thead>
    <?php
    $no=1;
    ?>
    @foreach($pasien as $p)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$p->id}}</td>
        <td>{{$p->NIK}}</td>
        <td>{{$p->username}}</td>
        <td>{{$p->telpon}}</td>
        <td>{{$p->jk}}</td>
        <td>{{$p->alamat}}</td>
    </tr>
    @endforeach
</table>
</div>
</div>
</div>
@endsection


