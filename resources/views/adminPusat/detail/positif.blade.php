@extends('layouts.admin')
@section('content')
    @if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif
<div class="card">
<div class="card-body">
<nav class="navbar navbar-light bg-light">
<h2>Daftar Kasus Pasien Positif Covid-19</h2>
<span>
<div class="d-flex">
  <div class="dropdown mr-1">
    <button type="button" class="btn btn-secondary dropdown-toggle" id="dropdownMenuOffset" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="10,20">
      Pilih Kondisi
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuOffset">
    <a class="dropdown-item" href="{{route('detail.positif')}}">All</a> 
    @foreach($no as $k)
      <a class="dropdown-item" href="/searchkondisi/detail/positif/{{$k->kondisi}}">
      @if($k->kondisi==1)
        Positif
      @elseif($k->kondisi==2)
        Meninggal
      @else
        Sembuh
      @endif
      </a>
    @endforeach
    </div>
  </div>
</span>
  <form class="form-inline" action="{{route('detail.search.positif')}}" method="get">
    <input class="form-control mr-sm-2" type="search" placeholder="Search Kode atau Nama" name="search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
</nav>
<div class="table-responsive">
<table class="table table-striped" width="100%" >
<thead>
    <tr>
        <th width="10%">NO</td>
        <th width="10%">Kode</th>
        <th width="30%">Nama</th>
        <th width="15%">Tanggal Positif</th>
        <th width="20%">Kondisi</th>
        <th width="15%">Tanggal Update</th> 
    </tr>
</thead>
    <?php
    $no=1;
    ?>
    @foreach($data as $d)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$d->penduduk_id}}</td>
        <td>{{$d->name}}</td>
        <td>{{$d->tanggal}}</td>
        <td>
        @if($d->kondisi==1)
            <p class="text-danger">Positif</p>
        @elseif($d->kondisi==2)
            <p class="text-secondary">Meninggal</p>
        @else
            <p class="text-success">Sembuh</p>
        @endif
        </td>
        <td>
        @if($d->tanggal_update == 0000-00-00)
            <p class="text-danger">Null</p>
        @else
            {{$d->tanggal_update}}
        @endif
    </td>
    </tr>
    @endforeach
</table>
</div>
<div class="font-italic">
Halaman Ke 
{{$data->currentPage()}}
Dari
{{$data->lastPage()}}
Halaman
</div>
<div>
{{$data->links()}}
</div>
</div>
</div>
@endsection


