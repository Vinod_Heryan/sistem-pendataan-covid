@extends('layouts.admin')
@section('content')
    @if ($message = Session::get('success'))
	  <div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
		  <strong>{{ $message }}</strong>
	  </div>
	@endif

	@if ($message = Session::get('update'))
	  <div class="alert alert-warning alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif

    @if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif
<div class="card">
<div class="card-body">
<nav class="navbar navbar-light bg-light">
<h2>Daftar Admin</h2>
  <form class="form-inline" action="{{route('admin.search')}}" method="get">
    <input class="form-control mr-sm-2" type="search" placeholder="Search Kode dan Nama" name="search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
</nav>
<div class="table-responsive">
<table class="table table-striped" width="100%">
<thead class="thead">
    <tr>
        <th width="10%">NO</td>
        <th width="15%">Kode Admin</th>
        <th width="20%">Nama Admin</th>
        <th width="20%">Email Admin</th>
        <th width="15%">Desa Bertugas</th>
        <th width="20%">Action</th>
    </tr>
</thead>
    <?php
    $no=1;
    ?>
    @foreach($data as $d)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$d->id}}</td>
        <td>{{$d->username}}</td>
        <td>{{$d->email}}</td>
        <td>{{$d->nama_desa}}</td>
        <td><a href="/update/admin/{{$d->id}}" class="btn btn-outline-warning my-2 my-sm-0">Ubah</a> | | <a href="/delete/admin/{{$d->id}}" class="btn btn-outline-danger my-2 my-sm-0">hapus</a></td>
    </tr>
    @endforeach
</table>
</div>
<div class="font-italic">
Halaman Ke 
{{$data->currentPage()}}
Dari
{{$data->lastPage()}}
Halaman
</div>
<div>
{{$data->links()}}
</div>
</div>
</div>
@endsection


