<head>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<title>Login Admin</title>
</head>
<!------ Include the above in your HEAD tag ---------->
@if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif
<body>
    <div id="login">
        <h3 class="text-center text-white pt-5">Login Admin</h3>
        <div class="d-flex justify-content-center">
        <img src="{{asset('tes/dist/img/7.png')}}" width="120px" height="120px" alt="AdminLTE Logo" class="brand-image img-circle elevation-3">
        </div>
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="{{ route('login') }}" method="post">
                        @csrf
                            <h3 class="text-center text-dark">Login</h3>
                            <div class="form-group">
                                <label for="email" class="text-dark">Email:</label><br>
                                <input id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-dark">Password:</label><br>
                                <input id="password" type="password" name="password" required autocomplete="current-password"class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-dark btn-md" value="Submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<style>
body {
  margin: 0;
  padding: 0;
  background-color: #2f4f4f;
  height: 100vh;
}
#login .container #login-row #login-column #login-box {
  margin-top: 80px;
  max-width: 600px;
  height: 320px;
  border: 1px solid #9C9C9C;
  background-color: #EAEAEA;
}
#login .container #login-row #login-column #login-box #login-form {
  padding: 20px;
}
#login .container #login-row #login-column #login-box #login-form #register-link {
  margin-top: -85px;
}
</style>


