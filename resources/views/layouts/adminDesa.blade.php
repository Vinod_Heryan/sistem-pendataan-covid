<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Welcome Admin Desa</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('tes/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('tes/dist/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-info navbar-light">
            <!-- Right navbar links -->
            <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="#" class="brand-link">
            <img src="{{asset('tes/dist/img/7.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3">
                <span class="brand-text font-weight-light">Admin Desa</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="{{asset('tes/dist/img/avatar5.png')}}" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">Hi Admin {{ Auth::user()->name }}</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                        <li class="nav-item">
                            <a href="/dashboard/desa/{{ Auth::user()->id }}" class="nav-link {{set_active('dashboard.desa')}}">
                                <i class="nav-icon fas fa-chart-bar"></i>
                                <p>
                                    Dashboard
                                </p>
                            </a>
                        </li>

                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-user-circle"></i>
                                <p>
                                 Umum
                                <i class="right fas fa-angle-left"></i>
                                </p>
                                </a>
                            <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('logout.manual')}}" class="nav-link">
                              <i class="far fa-circle nav-icon"></i>
                                <p>Logout</p>
                            </a>
                        </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview {{set_open(['penduduk','penduduk.search','penduduk.detail','hal.update.penduduk',
                          'inap','inap.search','positif','positif.search','sembuh','sembuh.search','vaksin','vaksin.search','vaksin.ke.search','kondisi.search',
                          'kondisi.inap.search','penduduk.search.desa'])}}">
                            <a href="#" class="nav-link {{set_active(['penduduk','penduduk.search','penduduk.detail','hal.update.penduduk',
                              'inap','inap.search','positif','positif.search','sembuh','sembuh.search','vaksin','vaksin.search','vaksin.ke.search','kondisi.search',
                              'kondisi.inap.search','penduduk.search.desa'])}}">
                                <i class="nav-icon fas fa-hospital"></i>
                                <p>
                                 Data Pasien
                                <i class="right fas fa-angle-left"></i>
                                </p>
                                </a>
                            <ul class="nav nav-treeview">
                            <li class="nav-item">
                <a href="{{route('penduduk')}}" class="nav-link {{set_active(['penduduk','penduduk.search','penduduk.detail','hal.update.penduduk','penduduk.search.desa'])}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Daftar Penduduk</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('inap')}}" class="nav-link {{set_active(['inap','inap.search','kondisi.inap.search'])}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Daftar Rawat Inap</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('positif')}}" class="nav-link {{set_active(['positif','positif.search','kondisi.search'])}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Daftar Kasus Positif</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('vaksin')}}" class="nav-link {{set_active(['vaksin','vaksin.search','vaksin.ke.search'])}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Daftar Pasien Vaksin</p>
                </a>
              </li>
            </ul>
          </li>

                        <li class="nav-item has-treeview {{set_open(['add.penduduk','add.positif','cekname',
                          'add.inap','cekname.inap','add.vaksin','cekname.vaksin'])}}">
                            <a href="#" class="nav-link {{set_active(['add.penduduk','add.positif','cekname',
                              'add.inap','cekname.inap','add.vaksin','cekname.vaksin'])}}">
                                <i class="nav-icon fas fa-file-signature"></i>
                                <p>
                                    Registrasi
                                <i class="right fas fa-angle-left"></i>
                                </p>
                                </a>
                            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('add.penduduk')}}" class="nav-link {{set_active('add.penduduk')}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tambah Data Penduduk</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('add.positif')}}" class="nav-link {{set_active(['add.positif','cekname'])}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tambah Pasien Positif</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('add.inap')}}" class="nav-link {{set_active(['add.inap','cekname.inap'])}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tambah Pasien Inap</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('add.vaksin')}}" class="nav-link {{set_active(['add.vaksin','cekname.vaksin'])}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tambah Pasien Vaksin</p>
                </a>
              </li>
            </ul>
          </li>
              <li class="nav-item has-treeview {{set_open(['pdf.positif','pdf.positif.search','pdf.inap','pdf.inap.search',
                'pdf.vaksin','pdf.vaksin.search'])}}">
                            <a href="#" class="nav-link {{set_active(['pdf.positif','pdf.positif.search','pdf.inap','pdf.inap.search',
                              'pdf.vaksin','pdf.vaksin.search'])}}">
                                <i class="nav-icon  fas fa-cloud-download-alt"></i>
                                <p>
                                 Download File PDF
                                <i class="right fas fa-angle-left"></i>
                                </p>
                                </a>
                            <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('pdf.positif')}}" class="nav-link {{set_active(['pdf.positif','pdf.positif.search'])}}">
                              <i class="far fa-circle nav-icon"></i>
                                <p>Pasien Positif</p>
                            </a>
                        </li>
                            </ul>
                            <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('pdf.inap')}}" class="nav-link {{set_active(['pdf.inap','pdf.inap.search'])}}">
                              <i class="far fa-circle nav-icon"></i>
                                <p>Pasien inap</p>
                            </a>
                        </li>
                            </ul>
                            <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('pdf.vaksin')}}" class="nav-link {{set_active(['pdf.vaksin','pdf.vaksin.search'])}}">
                              <i class="far fa-circle nav-icon"></i>
                                <p>Pasien Vaksin</p>
                            </a>
                        </li>
                            </ul>
                        </li>                
          
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>


        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>UTS PRPL</b>
            </div>
            <strong>Copyright &copy; Vinod Heryan (1842101641)
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="{{asset('tes/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('tes/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('tes/dist/js/adminlte.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('tes/dist/js/demo.js')}}"></script>
</body>

</html>