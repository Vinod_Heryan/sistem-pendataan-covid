<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Download File</title>
    <style>
.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

body {
  position: relative;
  width: 100%;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: #001028;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: Arial;
}

header {
  padding: 10px 0;
  margin-bottom: 30px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background: url(dimension.png);
}

#project span {
  color: #5D6975;
  text-align: right;
  width: 52px;
  margin-right: 10px;
  display: inline-block;
  font-size: 0.8em;
}

#company {
  float: left;
  text-align: right;
}

#project div,
#company div {
  white-space: nowrap;        
}

table {
  width: 90%;
  border-collapse: collapse;

  border-spacing: 0;
  margin-bottom: 20px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  text-align: center;
}

table th {
  padding: 5px 20px;
  color: #5D6975;
  border-bottom: 1px solid #C1CED9;
  white-space: nowrap;        
  font-weight: normal;
}

table .service,
table .desc {
  text-align: left;
}

table td {
  padding: 20px;
  text-align: center;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}
</style> 
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
      </div>
      <h1>Pasien Rawat Inap</h1>
      <div id="company" class="clearfix">
        <div>
        @foreach($tempat as $t)
        {{$t->tempat}}
        @endforeach  
        </div>
        <div>{{$tgl}}</div>
        <div>(602) 519-0450</div>
        <div>vinod.98572@gmail.com</div>
      </div>
    </header>
    <main>
    <table>
<thead>
    <tr>
        <th width="5%">Kode</th>
        <th width="15%">Nama</th>
        <th width="10%">Desa</th>
        <th width="25%">Alamat</th>
        <th width="10%">Tanggal Masuk</th>
        <th width="15%">Kondisi</th>
        <th width="10%">Tanggal Keluar</th>
    </tr>
</thead>
    @foreach($data as $d)
    <tr>
        <td>{{$d->penduduk_id}}</td>
        <td>{{$d->name}}</td>
        <td>{{$d->nama_desa}}</td>
        <td>{{$d->alamat}}</td>
        <td>{{$d->tanggal_masuk}}</td>
        <td>
        @if($d->kondisi==1)
            <p class="text-danger">Masih Rawat Inap</p>
        @else
            <p class="text-success">Sudah Keluar</p>
        @endif
        </td>
        <td>
        @if($d->tanggal_keluar == 0000-00-00)
            <p class="text-danger">-</p>
        @else
            {{$d->tanggal_keluar}}
        @endif
        </td>
    </tr>
    @endforeach
</table>
      <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">Berlaku salama 14 hari setelah pencetakan.</div>
      </div>
    </main>
  </body>

</html>