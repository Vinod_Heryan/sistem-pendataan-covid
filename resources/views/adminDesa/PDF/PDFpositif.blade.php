<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Download File</title>
    <style>
.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

body {
  position: relative;
  width: 100%;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: #001028;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: Arial;
}

header {
  padding: 10px 0;
  margin-bottom: 30px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background: url(dimension.png);
}

#project span {
  color: #5D6975;
  text-align: right;
  width: 52px;
  margin-right: 10px;
  display: inline-block;
  font-size: 0.8em;
}

#company {
  float: left;
  text-align: right;
}

#project div,
#company div {
  white-space: nowrap;        
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  text-align: center;
}

table th {
  padding: 5px 20px;
  color: #5D6975;
  border-bottom: 1px solid #C1CED9;
  white-space: nowrap;        
  font-weight: normal;
}

table .service,
table .desc {
  text-align: left;
}

table td {
  padding: 20px;
  text-align: center;
}

table td.service,
table td.desc {
  vertical-align: top;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table td.grand {
  border-top: 1px solid #5D6975;;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}
</style> 
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
      
      </div>
      <h1>Pasien Kasus Positif</h1>
      <div id="company" class="clearfix">
        <div>{{$tgl}}</div>
        <div>(602) 519-0450</div>
        <div>vinod.98572@gmail.com</div>
      </div>
    </header>
    <main>
    <table width="90%" >
<thead>
    <tr>
        <th width="10%">Kode</th>
        <th width="15%">Nama</th>
        <th width="10%">Desa</th>
        <th width="15%">Alamat</th>
        <th width="15%">Tanggal Positif</th>
        <th width="10%">Kondisi</th>
        <th width="15%">Tanggal Update</th>
    </tr>
</thead>
    @foreach($data as $d)
    <tr>
        <td>{{$d->penduduk_id}}</td>
        <td>{{$d->name}}</td>
        <td>{{$d->nama_desa}}</td>
        <td>{{$d->alamat}}</td>
        <td>{{$d->tanggal}}</td>
        <td>
        @if($d->kondisi==1)
            <p>Positif</p>
        @elseif($d->kondisi==2)
            <p>Meninggal</p>
        @else
            <p>Sembuh</p>
        @endif
        </td>
        <td>
        @if($d->tanggal_update == 0000-00-00)
            <p>Null</p>
        @else
            {{$d->tanggal_update}}
        @endif
        </td>
    </tr>
    @endforeach
</table>
      <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">Berlaku salama 14 hari setelah pencetakan.</div>
      </div>
    </main>
  </body>

</html>