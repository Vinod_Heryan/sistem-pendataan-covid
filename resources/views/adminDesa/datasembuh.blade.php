@extends('layouts.adminDesa')
@section('content')
    @if ($message = Session::get('success'))
	  <div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
		  <strong>{{ $message }}</strong>
	  </div>
	@endif

	@if ($message = Session::get('update'))
	  <div class="alert alert-warning alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif

    @if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif
<div class="card">
<div class="card-body">
<nav class="navbar navbar-light bg-light">
<h2>Daftar Pasien Yang Sudah Sembuh</h2>
  <form class="form-inline" action="{{route('sembuh.search')}}" method="get">
    <input class="form-control mr-sm-2" type="search" placeholder="Search Kode atau Nama" name="search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
</nav>
<div class="table-responsive">
<table class="table table-striped" width="100%" >
<thead>
    <tr>
        <th width="5%">NO</td>
        <th width="20%">Kode</th>
        <th width="35%">Nama</th>
        <th width="30%">Tanggal Sembuh</th>
        <th width="10%">Action</th>
    </tr>
</thead>
    <?php
    $no=1;
    ?>
    @foreach($data as $d)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$d->penduduk_id}}</td>
        <td>{{$d->name}}</td>
        <td>{{$d->tanggal}}</td>
        <td><a href="/delete/sembuh/{{$d->id}}" class="btn btn-outline-danger my-2 my-sm-0">Hapus</a></td>
    </tr>
    @endforeach
</table>
</div>
<div class="font-italic">
Halaman Ke 
{{$data->currentPage()}}
Dari
{{$data->lastPage()}}
Halaman
</div>
<div>
{{$data->links()}}
</div>
</div>
</div>
@endsection


