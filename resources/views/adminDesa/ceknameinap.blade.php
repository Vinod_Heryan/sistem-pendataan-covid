@extends('layouts.adminDesa')
@section('content')

    @if (count($errors) > 0)
    <div class="alert alert-danger">
             @foreach ($errors->all() as $error)
             <button type="button" class="close" data-dismiss="alert">×</button>	
              <li>ERROR!!! {{ $error }}</li>
             @endforeach
    </div>
@endif

<div class="card-header">
    <h2>Input Pasien Rawat Inap</h2>
</div>
<div class="card-body">
<table>
<form action="{{route('store.inap')}}" method="post">
{{csrf_field()}}
@foreach($data as $d)
    <div class="form-group">
        <label for="kode" class="col-2 col-form-label">Kode Pasien</label>
        <input type="number" name="penduduk_id" class="form-control" value="{{$d->id}}" placeholder="Masukan Kode" required="required" readonly>
    </div>
    <div class="form-group">
        <label for="nama-pasien" class="col-2 col-form-label">Nama Pasien</label>
        <input type="text" name="name" class="form-control" value="{{$d->name}}" placeholder="Masukan Nama" required="required" readonly>
    </div>
    <div class="form-group">
        <label for="nama-pasien" class="col-2 col-form-label">Tempat Rawat Inap</label>
        <textarea rows="3" cols="50" name="tempat" class="form-control" placeholder="Masukan Nama Tempat Rawat Inap" required="required"></textarea>
    </div>
    <div class="form-group">
        <label for="example-date-input" class="col-2 col-form-label">Tanggal </br> Masuk Rawat Inap</label>
        <input class="form-control" type="date" name="tanggal_masuk" id="example-date-input" required="required">
    </div>
@endforeach    
        <input type="submit" class="btn btn-primary" value="Simpan Data">
</form> 
</table>
</div>
@endsection