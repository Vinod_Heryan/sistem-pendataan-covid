@extends('layouts.adminDesa')
@section('content')
    @if ($message = Session::get('success'))
	  <div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
		  <strong>{{ $message }}</strong>
	  </div>
	@endif

	@if ($message = Session::get('update'))
	  <div class="alert alert-warning alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif

    @if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif
<div class="card"> 
<div class="card-body">
<nav class="navbar navbar-light bg-light"> 
<h2>Daftar Pasien Rawat Inap</h2>
<span>
<div class="d-flex">
  <div class="dropdown mr-1">
    <button type="button" class="btn btn-secondary dropdown-toggle" id="dropdownMenuOffset" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="10,20">
      Pilih Kondisi
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuOffset">
    <a class="dropdown-item" href="{{route('inap')}}">All</a> 
    @foreach($no as $k)
      <a class="dropdown-item" href="/searchkondisi/inap/{{$k->kondisi}}">
      @if($k->kondisi==1)
        Masih Rawat Inap
      @else
        Sembuh Keluar
      @endif
      </a>
    @endforeach
    </div>
  </div>
</span>
  <form class="form-inline" action="{{route('inap.search')}}" method="get">
    <input class="form-control mr-sm-2" type="search" placeholder="Search Kode atau Nama" name="search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
</nav>
<div class="table-responsive">
<table class="table table-striped" width="100%" >
<thead>
    <tr>
        <th width="5%">NO</td>
        <th width="8%">Kode</th>
        <th width="15%">Nama</th>
        <th width="19%">Tempat Rawat Inap</th>
        <th width="10%" class="text-center">Tanggal Masuk</th>
        <th width="15%">Kondisi</th>
        <th width="10%" class="text-center">Tanggal Keluar</th>
        <th width="18%" class="text-center">Action</th>
    </tr>
</thead>
    <?php
    $no=1;
    ?>
    @foreach($data as $d)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$d->penduduk_id}}</td>
        <td>{{$d->name}}</td>
        <td>{{$d->tempat}}</td>
        <td>{{$d->tanggal_masuk}}</td>
        <td>
        @if($d->kondisi==1)
            <p class="text-danger">Masih Rawat Inap</p>
        @else
            <p class="text-success">Sudah Keluar</p>
        @endif
        </td>
        <td>
        @if($d->tanggal_keluar == 0000-00-00)
            <p class="text-danger">Null</p>
        @else
            {{$d->tanggal_keluar}}
        @endif
        </td>
        @if($d->kondisi==1)
        <td class="d-flex justify-content-center">
            <a href="/update/inap/{{$d->id}}" class="btn btn-outline-danger my-2 my-sm-0">Keluar</a>
            &nbsp;&nbsp;&nbsp;<a href="/delete/inap/{{$d->id}}" class="btn btn-outline-danger my-2 my-sm-0">Hapus</a></td>
        @else
        <td class="d-flex justify-content-center">
            <a href="/delete/inap/{{$d->id}}" class="btn btn-outline-danger my-2 my-sm-0">Hapus</a>
        </td>
        @endif
    </tr>
    @endforeach
</table>
</div>
<div class="font-italic">
Halaman Ke 
{{$data->currentPage()}}
Dari
{{$data->lastPage()}}
Halaman
</div>
<div>
{{$data->links()}}
</div>
</div>
</div>
@endsection


