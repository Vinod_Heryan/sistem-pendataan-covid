@extends('layouts.adminDesa')
@section('content')
@if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif 

    @if (count($errors) > 0)
    <div class="alert alert-danger">
             @foreach ($errors->all() as $error)
             <button type="button" class="close" data-dismiss="alert">×</button>	
              <li>ERROR!!! {{ $error }}</li>
             @endforeach
    </div>
@endif
    
<div class="card-header">
    <h2>Pasien Positif</h2>
</div>
<div class="card-body">
<table>
<form action="{{route('cekname')}}" method="get">
{{csrf_field()}}
    <div class="form-group">
        <label for="Kode">Masukan Kode Pasien</label>
        <input type="number" name="id" class="form-control" placeholder="Masukan Kode" required="required">
        <small class="form-text text-muted">Kode Harus Terdaftar Di Daftar Pasien</small>
    </div>
        <input type="submit" class="btn btn-primary" value="Cek Data">
</form> 
</table>
</div>
@endsection