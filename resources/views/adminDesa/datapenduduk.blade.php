@extends('layouts.adminDesa')
@section('content')
    @if ($message = Session::get('success'))
	  <div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
		  <strong>{{ $message }}</strong>
	  </div>
	@endif

	@if ($message = Session::get('update'))
	  <div class="alert alert-warning alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif

    @if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif  
<div class="card">
<div class="card-body">
<nav class="navbar navbar-light bg-light">
<h2>Daftar Penduduk</h2>
<span>
<div class="d-flex">
  <div class="dropdown mr-1">
    <button type="button" class="btn btn-secondary dropdown-toggle" id="dropdownMenuOffset" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="10,20">
      Pilih Desa
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuOffset">
    <a class="dropdown-item" href="{{route('penduduk')}}">All</a>
    @foreach($desa as $d)
      <a class="dropdown-item" href="/search/penduduk/{{$d->name}}">Desa {{$d->name}}</a>
    @endforeach
    </div>
  </div> 
</span>
  <form class="form-inline" action="{{route('penduduk.search')}}" method="get">
    <input class="form-control mr-sm-2" type="search" placeholder="Search Kode atau Nama" name="search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
</nav>
<div class="table-responsive">
<table class="table table-striped" width="100%" >
<thead>
    <tr>
        <th width="5%">NO</td>
        <th width="10%">Kode</th>
        <th width="35%">Nama</th>
        <th width="20%">Jenis Kelamin</th>
        <th width="20%">Asal Desa</th>
        <th width="10%">Action</th>
    </tr>
</thead>
    <?php
    $no=1;
    ?>
    @foreach($data as $d)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$d->id}}</td>
        <td>{{$d->username}}</td>
        <td>{{$d->jk}}</td>
        <td>{{$d->nama_desa}}</td>
        <td><a href="/detail/penduduk/{{$d->id}}" class="btn btn-outline-primary my-2 my-sm-0">Detail</a></td>
    </tr>
    @endforeach
</table>
</div>
<div class="font-italic">
Halaman Ke 
{{$data->currentPage()}}
Dari
{{$data->lastPage()}}
Halaman
</div>
<div>
{{$data->links()}}
</div>
</div>
</div>
@endsection


