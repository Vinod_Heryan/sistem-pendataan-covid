@extends('layouts.adminDesa')
@section('content')

    @if (count($errors) > 0)
    <div class="alert alert-danger">
             @foreach ($errors->all() as $error)
             <button type="button" class="close" data-dismiss="alert">×</button>	
              <li>ERROR!!! {{ $error }}</li>
             @endforeach
    </div>
@endif

<div class="card-header">
    <h2>Tambah Data Penduduk</h2>
</div>
<div class="card-body">
<table>
<form action="{{route('store.penduduk')}}" method="post">
{{csrf_field()}}
    <div class="form-group">
        <label for="NIK">NIK</label>
        <input type="number" name="NIK" class="form-control" placeholder="Masukan No NIK" required="required">
        <small class="form-text text-muted">Format Angka</small>
    </div>
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" name="name" class="form-control" placeholder="Masukan Nama" required="required">
        <small class="form-text text-muted">Jika Inputan Terlalu Panjang Disarankan Untuk Disingkat</small>
    </div>
    <div class="form-group">
        <label for="telpon">No Telpon</label>
        <input type="number" name="telpon" class="form-control" placeholder="Masukan No Telpon" required="required">
        <small class="form-text text-muted">Format Angka</small>
    </div>
    <div class="form-group">
        <label for="jk">Jenis Kelamin</label>
        <select name="jk" class="custom-select" required="required">
            <option value="" selected>--Pilih Jenis Kelamin--</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
            </select>
    </div>
    <div class="form-group">
        <label for="desa">Desa</label>
        <select name="desa_id" class="custom-select" required="required">
            <option value="" selected>--Pilih Desa--</option>
            @foreach($data as $d)
            <option value="{{$d->id}}">{{$d->name}}</option>
            @endforeach
            </select>
    </div>
    <div class="form-group">
        <label for="a" >Alamat</label>
        <textarea rows="3" cols="50" name="alamat" class="form-control" placeholder="Masukan Alamat" required="required"></textarea>
    </div>
        <input type="submit" class="btn btn-primary" value="Simpan Data">
</form> 
</table>
</div>
@endsection