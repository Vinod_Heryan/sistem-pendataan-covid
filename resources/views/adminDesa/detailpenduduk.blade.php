@extends('layouts.adminDesa')
@section('content')
    @if ($message = Session::get('success'))
	  <div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
		  <strong>{{ $message }}</strong>
	  </div>
	@endif

	@if ($message = Session::get('update'))
	  <div class="alert alert-warning alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif

    @if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif
<div class="card">
<div class="card-body" >
<div class="table-responsive">
<table class="table table-striped" width="100%" >
<thead>
    <tr>
        <th width="10%">Kode</td>
        <th width="10%">NIK</th>
        <th width="15%">Nama</th>
        <th width="10%">No Telpon</th>
        <th width="5%">Jenis Kelamin</th>
        <th width="20%">Alamat</th>
        <th width="10%">Asal Desa</th>
        <th width="20%">Action</th>
    </tr>
</thead>
    @foreach($data as $d)
    <tr>
        <td>{{$d->id}}</td>
        <td>{{$d->NIK}}</td>
        <td>{{$d->username}}</td>
        <td>{{$d->telpon}}</td>
        <td>{{$d->jk}}</td>
        <td>{{$d->alamat}}</td>
        <td>{{$d->nama_desa}}</td>
        <td><a href="/update/penduduk/{{$d->id}}" class="btn btn-outline-warning my-2 my-sm-0">Ubah</a> 
        | | <a href="/delete/penduduk/{{$d->id}}" class="btn btn-outline-danger my-2 my-sm-0">Hapus</a></td>
    </tr>
    @endforeach
</table>
</div>
</div>
</div>
@endsection


