@extends('layouts.adminDesa')
@section('content')

    @if (count($errors) > 0)
    <div class="alert alert-danger">
             @foreach ($errors->all() as $error)
             <button type="button" class="close" data-dismiss="alert">×</button>	
              <li>ERROR!!! {{ $error }}</li>
             @endforeach
    </div>
@endif

<div class="card-header">
    <h2>Input Pasien Vaksin</h2>
</div>
<div class="card-body">
<table>
<form action="{{route('store.vaksin')}}" method="post">
{{csrf_field()}}
@foreach($data as $d)
    <div class="form-group">
        <label for="kode" class="col-2 col-form-label">Kode Pasien</label>
        <input type="number" name="penduduk_id" class="form-control" value="{{$d->id}}" placeholder="Masukan Kode" required="required" readonly>
    </div>
    <div class="form-group">
        <label for="nama-pasien" class="col-2 col-form-label">Nama Pasien</label>
        <input type="text" name="name" class="form-control" value="{{$d->name}}" placeholder="Masukan Nama" required="required" readonly>
    </div>
    <div class="form-group">
        <label for="nama-pasien" class="col-2 col-form-label">Vaksin Ke</label>
        <input type="number" name="nama_vaksin" class="form-control" placeholder="Masukan Nama Vaksin Ke Beberapa" required="required">
    </div>
    <div class="form-group">
        <label for="nama-pasien" class="col-2 col-form-label">Nama Jenis Vaksin</label>
        <input type="text" name="jenis_vaksin" class="form-control" placeholder="Masukan Nama Jenis Vaksin" required="required">
    </div>
    <div class="form-group">
        <label for="example-date-input" class="col-2 col-form-label">Tanggal Vaksin</label>
        <input class="form-control" type="date" name="tanggal" id="example-date-input" required="required">
    </div>
@endforeach    
        <input type="submit" class="btn btn-primary" value="Simpan Data">
</form> 
</table>
</div>
@endsection