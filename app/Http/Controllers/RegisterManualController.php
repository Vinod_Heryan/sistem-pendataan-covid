<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;


class RegisterManualController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_admin');
    }

    public function halamanregister()
    {
        $data = DB::table('desa')
        ->select('id','name')
        ->get();

        return view('adminPusat/manualregister', compact('data'));
    }

    public function index()
    {
        $data = DB::table('users')
        ->select('users.id','users.name as username','users.email','users.desa_id','desa.name as nama_desa')
        ->join('desa','users.desa_id','=','desa.id')
        ->orderBy('desa.name','ASC')
        ->paginate(5);

        return view('adminPusat/dataadmin', compact('data'));
    }

    public function search(Request $request)
    {
        $data = DB::table('users')
        ->select('users.id','users.name as username','users.email','users.desa_id','desa.name as nama_desa')
        ->join('desa','users.desa_id','=','desa.id')
        ->where('users.name', 'like', "%{$request->search}%")
        ->orwhere('users.id', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('adminPusat/dataadmin',compact('data'));
    }

    public function register(Request $request)
    {
        try{
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:8',
            'is_admin' => 'required|string',
            'desa_id' => 'required|string',
        ]);

        $table = new User();
        $table->name=$request->name;
        $table->email=$request->email;
        $table->password= Hash::make($request->password);
        $table->is_admin=$request->is_admin;
        $table->desa_id=$request->desa_id;
        $table->save();
        }catch(\Exception $e){

            return redirect()->back()->with('error','Register Admin Gagal!!! Inputan Tidak Valid');

        }

        return redirect('admin')->with('success','Register Admin Berhasil');
    }

    public function ubah($id)
    {
        $user = DB::table('users')
        ->select('users.id','users.name as username','users.email','is_admin','users.desa_id','desa.name as nama_desa')
        ->join('desa','users.desa_id','=','desa.id')
        ->where('users.id',$id)
        ->get();

        $data = DB::table('desa')
        ->select('id','name')
        ->get();

        return view('adminPusat/updateadmin', compact('data','user'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|min:8',
            'is_admin' => 'required',
            'desa_id' => 'required',
        ]);

        $table = User::where('id',$request->id)->update([
            'id'=> $request->id,
            'name'=> $request->name,
            'email'=> $request->email,
            'is_admin' => $request->is_admin,
            'password'=> Hash::make($request->password),
            'desa_id'=> $request->desa_id,
        ]);

        return redirect()->route('admin')->with('success', 'Data Admin Berhasi Diupdate');
    }

    public function hapus($id)
    { 
        try{
            $hapus = User::find($id)->delete();
        }catch(\Exception $e){
            return redirect()->route('admin')->with('error', 'Data Admin Tidak Bisa Dihapus!!!!');
        } 
        return redirect()->route('admin')->with('success', 'Data Admin Berhasi Dihapus');  
    }
}
