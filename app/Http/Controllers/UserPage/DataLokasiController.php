<?php

namespace App\Http\Controllers\UserPage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Desa;
use Illuminate\Support\Facades\DB;

class DataLokasiController extends Controller
{
    public function index()
    {
        $table = Desa::paginate(5);

        return view('userPage/datalokasi',compact('table'));
    }

    public function search(Request $request)
    {
        $table = Desa::where('name', 'like', "%{$request->search}%")
        ->orwhere('id', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('userPage/datalokasi',compact('table'));
    }

    public function laporan($id)
    {
        $data = Desa::where('id',$id)->get();

        $penduduk = DB::table('penduduk')
        ->select('penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('desa.id', $id)->count();

        $positif = DB::table('positif')
        ->select('positif.id')
        ->join('penduduk','positif.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('desa.id', $id)->count();
 
        $sembuh = DB::table('positif')
        ->select('positif.id')
        ->join('penduduk','positif.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('desa.id', $id)
        ->where('kondisi', '0')->count();

        $meninggal = DB::table('positif')
        ->select('positif.id')
        ->join('penduduk','positif.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('desa.id', $id)
        ->where('kondisi', '2')->count();

        $vaksin = DB::table('vaksin')
        ->select('vaksin.id')
        ->join('penduduk','vaksin.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('desa.id', $id)->count();

        $inap = DB::table('inap')
        ->select('inap.id')
        ->join('penduduk','inap.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('desa.id', $id)
        ->where('inap.kondisi', 1)->count();

        return view('userPage/laporan', compact('penduduk','positif','sembuh','vaksin','inap','meninggal','data'));
    }
}
