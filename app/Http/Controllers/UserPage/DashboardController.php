<?php

namespace App\Http\Controllers\UserPage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Inap;
use App\Models\Desa;
use App\Models\Positif;
use App\Models\Sembuh;
use App\Models\Vaksin;
use App\Models\Penduduk;

class DashboardController extends Controller
{
    public function index()
    {
        $penduduk = Penduduk::all()->count();

        $positif = Positif::all()->count();

        $sembuh = Positif::where('kondisi','0')->count();

        $vaksin = Vaksin::all()->count();

        $inap = Inap::all()->where('kondisi', 1)->count();

        $meninggal = Positif::where('kondisi','2')->count(); 

        return view('userPage/dashboard', compact('penduduk','positif','sembuh','vaksin','inap','meninggal'));
    }
}
