<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Desa;
use Illuminate\Support\Facades\DB;

class DesaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_admin');
    }
    
    public function index()
    {
        $table = Desa::paginate(5);

        return view('adminPusat/datadesa',compact('table'));
    }

    public function search(Request $request)
    {
        $table = Desa::where('name', 'like', "%{$request->search}%")
        ->orwhere('id', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('adminPusat/datadesa',compact('table'));
    }

    public function add()
    {
        return view('adminPusat/adddesa');
    }

    public function store(Request $request)
    {
       $request->validate([
           'name' => 'required|string',
           'alamat' => 'required|string',
       ]);

        Desa::create($request->all());

       return redirect()->route('desa')->with('success','Data Desa Berhasi Ditambahkan');
    }

    public function ubah($id)
    {
        $table = Desa::where('id',$id)->get();
        
        return view('adminPusat/updatedesa',compact('table'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'name' => 'required|string',
            'alamat' => 'required|string',
        ]);

        $table = Desa::where('id',$request->id)->update([
            'id'=>$request->id,
            'name'=> $request->name,
            'alamat'=> $request->alamat,
        ]);

        return redirect()->route('desa')->with('success', 'Data Desa Berhasi Diupdate');
    }

    public function hapus($id)
    { 
        try{
            $hapus = Desa::find($id)->delete();
        }catch(\Exception $e){
            return redirect()->route('desa')->with('error', 'Data Desa Ini Tidak Bisa Dihapus!!!!');
        } 
        return redirect()->route('desa')->with('success', 'Data Desa Berhasi Dihapus');  
    }
}
