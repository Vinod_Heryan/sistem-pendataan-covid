<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inap;
use App\Models\Desa;
use App\Models\Positif;
use App\Models\Sembuh;
use App\Models\Vaksin;
use App\Models\Penduduk;
use Illuminate\Support\Facades\DB;

class DetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_admin');
    }

    //=================================================================================================

    public function searchPasien(Request $request)
    {
        $desa = DB::table('penduduk')
        ->select('desa.name')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->groupBy('desa.name')
        ->get();

        $pasien = DB::table('penduduk')
        ->select('penduduk.id','penduduk.name as username',
        'penduduk.jk','desa.name as nama_desa')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('penduduk.name', 'like', "%{$request->search}%")
        ->orwhere('penduduk.id', 'like', "%{$request->search}%")
        ->orwhere('desa.name', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('adminPusat/detail/pasien', compact('pasien','desa'));
    }

    public function pasien()
    {
        $desa = DB::table('penduduk')
        ->select('desa.name')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->groupBy('desa.name')
        ->get();

        $pasien = DB::table('penduduk')
        ->select('penduduk.id','penduduk.name as username',
        'penduduk.jk','desa.name as nama_desa','desa_id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->paginate(5);
        
        return view('adminPusat/detail/pasien', compact('pasien','desa'));
    }

    public function detailPasien($id)
    {
        $desa = DB::table('penduduk')
        ->select('desa.name')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->groupBy('desa.name')
        ->get();

        $pasien = DB::table('penduduk')
        ->select('penduduk.id','penduduk.name as username',
        'NIK','penduduk.telpon','penduduk.jk','penduduk.alamat',
        'desa.name as nama_desa')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('penduduk.id',$id)->get();
        
        return view('adminPusat/detail/detailpasien', compact('pasien','desa'));
    }

    public function searchdesa($nama_desa)
    {
        $desa = DB::table('penduduk')
        ->select('desa.name')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->groupBy('desa.name')
        ->get();

        $pasien = DB::table('penduduk')
        ->select('penduduk.id','penduduk.name as username','penduduk.jk','penduduk.desa_id','desa.name as nama_desa')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('desa.name', $nama_desa)
        ->paginate(5);

        return view('adminPusat/detail/pasien',compact('pasien','desa'));
    }

    //================================================================================================

    public function vaksin()
    {
        $vaksin = DB::table('vaksin')
        ->select('vaksin.id','vaksin.nama_vaksin')
        ->groupBy('vaksin.nama_vaksin')
        ->orderBy('vaksin.nama_vaksin','ASC')
        ->get();

        $data = DB::table('vaksin')
        ->select('vaksin.id','vaksin.nama_vaksin','vaksin.penduduk_id','penduduk.name','vaksin.tanggal','vaksin.jenis_vaksin')
        ->join('penduduk','vaksin.penduduk_id','=','penduduk.id')
        ->paginate(5);

        return view('adminPusat/detail/vaksin', compact('data','vaksin'));
    }

    public function searchVaksin($name_vaksin)
    {
        $vaksin = DB::table('vaksin')
        ->select('vaksin.id','vaksin.nama_vaksin')
        ->groupBy('vaksin.nama_vaksin')
        ->orderBy('vaksin.nama_vaksin','ASC')
        ->get();

        $data = DB::table('vaksin')
        ->select('vaksin.id','vaksin.nama_vaksin','vaksin.penduduk_id','penduduk.name','vaksin.tanggal','vaksin.jenis_vaksin')
        ->join('penduduk','vaksin.penduduk_id','=','penduduk.id')
        ->where('vaksin.nama_vaksin', $name_vaksin)
        ->paginate(5);

        return view('adminPusat/detail/vaksin', compact('data','vaksin'));
    }

    public function searchDetailVaksin(Request $request)
    {
        $vaksin = DB::table('vaksin')
        ->select('vaksin.id','vaksin.nama_vaksin')
        ->groupBy('vaksin.nama_vaksin')
        ->orderBy('vaksin.nama_vaksin','ASC')
        ->get();

        $data = DB::table('vaksin')
        ->select('vaksin.id','vaksin.nama_vaksin','vaksin.penduduk_id','penduduk.name','vaksin.tanggal','vaksin.jenis_vaksin')
        ->join('penduduk','vaksin.penduduk_id','=','penduduk.id')
        ->where('penduduk.name', 'like', "%{$request->search}%")
        ->orwhere('vaksin.penduduk_id', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('adminPusat/detail/vaksin', compact('data','vaksin'));
    }

    //================================================================================================

    public function positif()
    {
        $no = DB::table('positif')
        ->select('positif.id','positif.kondisi')
        ->groupBy('positif.kondisi')
        ->orderBy('positif.kondisi','ASC')
        ->get();

        $data = DB::table('positif')
        ->select('positif.id','positif.penduduk_id','penduduk.name','positif.tanggal','positif.kondisi',
        'positif.tanggal_update')
        ->join('penduduk','positif.penduduk_id','=','penduduk.id')
        ->orderBy('positif.kondisi','ASC')
        ->paginate(5);

        return view('adminPusat/detail/positif', compact('data','no')); 
    }

    public function searchkondisi($kondisi)
    {
        $no = DB::table('positif')
        ->select('positif.id','positif.kondisi')
        ->groupBy('positif.kondisi')
        ->orderBy('positif.kondisi','ASC')
        ->get();

        $data = DB::table('positif')
        ->select('positif.id','positif.penduduk_id','penduduk.name','positif.tanggal','positif.kondisi',
        'positif.tanggal_update')
        ->join('penduduk','positif.penduduk_id','=','penduduk.id')
        ->where('positif.kondisi', $kondisi)
        ->paginate(5);

        return view('adminPusat/detail/positif', compact('data','no'));
    }

    public function searchPositif(Request $request)
    {
        $no = DB::table('positif')
        ->select('positif.id','positif.kondisi')
        ->groupBy('positif.kondisi')
        ->orderBy('positif.kondisi','ASC')
        ->get();
        
        $data = DB::table('positif')
        ->select('positif.id','positif.penduduk_id','penduduk.name','positif.tanggal','positif.kondisi',
        'positif.tanggal_update')
        ->join('penduduk','positif.penduduk_id','=','penduduk.id')
        ->where('penduduk.name', 'like', "%{$request->search}%")
        ->orwhere('penduduk.id', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('adminPusat/detail/positif', compact('data','no'));
    }

    //================================================================================================

    public function sembuh()
    {
        $data = DB::table('sembuh')
        ->select('sembuh.id','sembuh.penduduk_id','penduduk.name','sembuh.tanggal')
        ->join('penduduk','sembuh.penduduk_id','=','penduduk.id')
        ->paginate(5);

        return view('adminPusat/detail/sembuh', compact('data'));
    }

    public function searchSembuh(Request $request)
    {
        $data = DB::table('sembuh')
        ->select('sembuh.id','sembuh.penduduk_id','penduduk.name','sembuh.tanggal')
        ->join('penduduk','sembuh.penduduk_id','=','penduduk.id')
        ->where('penduduk.name', 'like', "%{$request->search}%")
        ->orwhere('penduduk.id', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('adminPusat/detail/sembuh', compact('data'));
    }

    //================================================================================================

    public function inap()
    {
        $no = DB::table('inap')
        ->select('inap.id','inap.kondisi')
        ->groupBy('inap.kondisi')
        ->get();

        $data = DB::table('inap')
        ->select('inap.id','inap.penduduk_id','penduduk.name','inap.tanggal_masuk','inap.kondisi','inap.tanggal_keluar','inap.tempat')
        ->join('penduduk','inap.penduduk_id','=','penduduk.id')
        ->orderBy('inap.kondisi','ASC')
        ->paginate(5);

        return view('adminPusat/detail/inap',compact('data','no'));
    }

    public function searchinapkondisi($id)
    {
        $no = DB::table('inap')
        ->select('inap.id','inap.kondisi')
        ->groupBy('inap.kondisi')
        ->get();

        $data = DB::table('inap')
        ->select('inap.id','inap.penduduk_id','penduduk.name','inap.tanggal_masuk','inap.kondisi','inap.tanggal_keluar','inap.tempat')
        ->join('penduduk','inap.penduduk_id','=','penduduk.id')
        ->where('inap.kondisi', $id)
        ->paginate(5);

        return view('adminPusat/detail/inap',compact('data','no'));
    }

    public function searchInap(Request $request)
    {
        $no = DB::table('inap')
        ->select('inap.id','inap.kondisi')
        ->groupBy('inap.kondisi')
        ->get();

        $data = DB::table('inap')
        ->select('inap.id','inap.penduduk_id','penduduk.name','inap.tanggal_masuk','inap.kondisi','inap.tanggal_keluar','inap.tempat')
        ->join('penduduk','inap.penduduk_id','=','penduduk.id')
        ->where('penduduk.name', 'like', "%{$request->search}%")
        ->orwhere('penduduk.id', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('adminPusat/detail/inap',compact('data','no'));
    }

}
