<?php

namespace App\Http\Controllers\AdminDesa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;


class PDFDesaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_desa');
    }

    //Kasus positif=====================================================================================================================================

    public function positif()
    {
        $data = DB::table('positif')
        ->select('positif.id','positif.penduduk_id','penduduk.name as nama_penduduk','penduduk.jk','desa.name as nama_desa','penduduk.alamat')
        ->join('penduduk','positif.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->paginate(5);

        return view('adminDesa/PDF/PDFdatapositif',compact('data'));
    }

    public function searchpositif(Request $request)
    {
        $data = DB::table('positif')
        ->select('positif.id','positif.penduduk_id','penduduk.name as nama_penduduk','penduduk.jk','desa.name as nama_desa','penduduk.alamat')
        ->join('penduduk','positif.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('penduduk.name', 'like', "%{$request->search}%")
        ->orwhere('penduduk.id', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('adminDesa/PDF/PDFdatapositif',compact('data'));
    }

    public function downloadpositif($id)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = DB::table('positif')
        ->select('positif.id','positif.penduduk_id','penduduk.name','positif.tanggal','positif.kondisi',
        'positif.tanggal_update','desa.name as nama_desa','penduduk.alamat')
        ->join('penduduk','positif.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('positif.id',$id)
        ->get();

        $tgl=date('Y-m-d');

        $pdf = PDF::loadview('adminDesa/PDF/PDFpositif',['data'=>$data,'tgl'=>$tgl]);

        return $pdf->download('File-Positif.pdf');
    }
    
    //Kasus inap=====================================================================================================================================

    public function inap()
    {
        $data = DB::table('inap')
        ->select('inap.id','inap.penduduk_id','penduduk.name as nama_penduduk','penduduk.jk','desa.name as nama_desa','penduduk.alamat')
        ->join('penduduk','inap.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->paginate(5);

        return view('adminDesa/PDF/PDFdatainap',compact('data'));
    }

    public function searchinap(Request $request)
    {
        $data = DB::table('inap')
        ->select('inap.id','inap.penduduk_id','penduduk.name as nama_penduduk','penduduk.jk','desa.name as nama_desa','penduduk.alamat')
        ->join('penduduk','inap.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('penduduk.name', 'like', "%{$request->search}%")
        ->orwhere('penduduk.id', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('adminDesa/PDF/PDFdatainap',compact('data'));
    }

    public function downloadinap($id)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = DB::table('inap')
        ->select('inap.id','inap.penduduk_id','penduduk.name','inap.tanggal_masuk','inap.kondisi',
        'inap.tanggal_keluar','inap.tempat','desa.name as nama_desa','penduduk.alamat')
        ->join('penduduk','inap.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('inap.id',$id)
        ->get();

        $tempat = DB::table('inap')
        ->select('inap.id','inap.tempat')
        ->join('penduduk','inap.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('inap.id',$id)
        ->get();


        $tgl=date('Y-m-d');

        $pdfinap = PDF::loadview('adminDesa/PDF/PDFinap',['data'=>$data,'tgl'=>$tgl,'tempat'=>$tempat]);

        return $pdfinap->download('File-Rawat-Inap.pdf');
    }

    //Kasus Vaksin=====================================================================================================================================

    public function vaksin()
    {
        $data = DB::table('vaksin')
        ->select('vaksin.id','vaksin.penduduk_id','penduduk.name as nama_penduduk','penduduk.jk','desa.name as nama_desa',
        'penduduk.alamat','vaksin.nama_vaksin')
        ->join('penduduk','vaksin.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->orderBy('vaksin.nama_vaksin','ASC')
        ->paginate(5);

        return view('adminDesa/PDF/PDFdatavaksin',compact('data'));
    }

    public function searchvaksin(Request $request)
    {
        $data = DB::table('vaksin')
        ->select('vaksin.id','vaksin.penduduk_id','penduduk.name as nama_penduduk','penduduk.jk','desa.name as nama_desa',
        'penduduk.alamat','vaksin.nama_vaksin')
        ->join('penduduk','vaksin.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('penduduk.name', 'like', "%{$request->search}%")
        ->orwhere('penduduk.id', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('adminDesa/PDF/PDFdatavaksin',compact('data'));
    }

    public function downloadvaksin($id)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = DB::table('vaksin')
        ->select('vaksin.id','vaksin.penduduk_id','penduduk.name','vaksin.tanggal',
        'desa.name as nama_desa','penduduk.alamat')
        ->join('penduduk','vaksin.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('vaksin.id',$id)
        ->get();

        $vaksin = DB::table('vaksin')
        ->select('vaksin.id','vaksin.nama_vaksin')
        ->join('penduduk','vaksin.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('vaksin.id',$id)
        ->get();

        $nama = DB::table('vaksin')
        ->select('vaksin.id','vaksin.jenis_vaksin')
        ->join('penduduk','vaksin.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('vaksin.id',$id)
        ->get();

        $tgl=date('Y-m-d');

        $pdf = PDF::loadview('adminDesa/PDF/PDFvaksin',['data'=>$data,'tgl'=>$tgl,'vaksin'=>$vaksin,'nama'=>$nama]);

        return $pdf->download('File-vaksin.pdf');
    }
}
