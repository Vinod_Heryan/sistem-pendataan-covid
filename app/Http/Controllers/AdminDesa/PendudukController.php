<?php

namespace App\Http\Controllers\AdminDesa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Desa;
use App\Models\Penduduk;
use Illuminate\Support\Facades\DB;

class PendudukController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_desa');
    }

    public function index()
    {
        $desa = DB::table('penduduk')
        ->select('desa.name')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->groupBy('desa.name')
        ->get();

        $data = DB::table('penduduk')
        ->select('penduduk.id','penduduk.name as username','penduduk.jk','penduduk.desa_id','desa.name as nama_desa')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->orderBy('penduduk.name','ASC')
        ->paginate(5);

        return view('adminDesa/datapenduduk',compact('data','desa'));
    }

    public function search(Request $request)
    {
        $desa = DB::table('penduduk')
        ->select('desa.name')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->groupBy('desa.name')
        ->get();

        $data = DB::table('penduduk')
        ->select('penduduk.id','penduduk.name as username','penduduk.jk','penduduk.desa_id','desa.name as nama_desa')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('penduduk.name', 'like', "%{$request->search}%")
        ->orwhere('penduduk.id', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('adminDesa/datapenduduk',compact('data','desa'));
    }

    public function searchdesa($nama_desa)
    {
        $desa = DB::table('penduduk')
        ->select('desa.name')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->groupBy('desa.name')
        ->get();

        $data = DB::table('penduduk')
        ->select('penduduk.id','penduduk.name as username','penduduk.jk','penduduk.desa_id','desa.name as nama_desa')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('desa.name', $nama_desa)
        ->paginate(5);

        return view('adminDesa/datapenduduk',compact('data','desa'));
    }


    public function detail($id)
    {
        $data = DB::table('penduduk')
        ->select('penduduk.id','penduduk.NIK','penduduk.name as username','penduduk.telpon','penduduk.jk','penduduk.alamat','penduduk.desa_id','desa.name as nama_desa')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('penduduk.id',$id)
        ->paginate(1); 

        return view('adminDesa/detailpenduduk',compact('data'));
    }

    public function add()
    {
        $data = Desa::all();
        return view('adminDesa/addpenduduk',compact('data'));
    }

    public function store(Request $request)
    {
      
        $request->validate([
            'NIK' => 'required|numeric',
            'name' => 'required',
            'telpon' => 'required|numeric',
            'jk' => 'required',
            'alamat' => 'required',
            'desa_id' => 'required',
        ],[
            'NIK.required' =>'Data NIK Tidak Boleh Kosong',
            'name.required' =>'Data Nama Tidak Boleh Kosong',
            'telpon.required' =>'Data Telpon Tidak Boleh Kosong',
            'jk.required' =>'Data Jenis Kelamin Tidak Boleh Kosong',
            'alamat.required' =>'Data Alamat Tidak Boleh Kosong',
            'desa_id.required' =>'Data Desa Tidak Boleh Kosong',
            'NIK.numeric' =>'Data NIK Harus Dalam Format Angka',
            'telpon.numeric' =>'Data Telpon Harus Dalam Format Angka',
        ]);
    
            $table = new Penduduk();
            $table->NIK=$request->NIK;
            $table->name=$request->name;
            $table->telpon=$request->telpon;
            $table->jk=$request->jk;
            $table->alamat=$request->alamat;
            $table->desa_id=$request->desa_id;
            $table->save();
       
    
            return redirect('penduduk')->with('success','Tambah Data Penduduk Berhasil');
    }

    public function ubah($id)
    {
        $penduduk = DB::table('penduduk')
        ->select('penduduk.id','penduduk.NIK','penduduk.name as username','penduduk.telpon','penduduk.jk','penduduk.alamat','penduduk.desa_id','desa.name as nama_desa')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->where('penduduk.id',$id)
        ->get();

        $desa = DB::table('desa')
        ->select('id','name')
        ->get();

        return view('adminDesa/updatependuduk', compact('desa','penduduk'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'NIK' => 'required|numeric',
            'name' => 'required',
            'telpon' => 'required|numeric',
            'jk' => 'required',
            'alamat' => 'required',
            'desa_id' => 'required',
        ],[
            'NIK.required' =>'Data NIK Tidak Boleh Kosong',
            'name.required' =>'Data Nama Tidak Boleh Kosong',
            'telpon.required' =>'Data Telpon Tidak Boleh Kosong',
            'jk.required' =>'Data Jenis Kelamin Tidak Boleh Kosong',
            'alamat.required' =>'Data Alamat Tidak Boleh Kosong',
            'desa_id.required' =>'Data Desa Tidak Boleh Kosong',
            'NIK.numeric' =>'Data NIK Harus Dalam Format Angka',
            'telpon.numeric' =>'Data Telpon Harus Dalam Format Angka',
        ]);

        $table = Penduduk::where('id',$request->id)->update([
            'id'=> $request->id,
            'NIK'=>$request->NIK,
            'name'=>$request->name,
            'telpon'=>$request->telpon,
            'jk'=>$request->jk,
            'alamat'=>$request->alamat,
            'desa_id'=>$request->desa_id,
        ]);

        return redirect()->route('penduduk')->with('success', 'Data Pasien Berhasi Diupdate');
    }

    public function hapus($id)
    { 
        try{
            $hapus = Penduduk::find($id)->delete();
        }catch(\Exception $e){
            return redirect()->route('penduduk')->with('error', 'Data Pasien Tidak Bisa Dihapus!!!!');
        } 
        return redirect()->route('penduduk')->with('success', 'Data Pasien Berhasi Dihapus');  
    }
}

