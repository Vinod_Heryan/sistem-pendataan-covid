<?php

namespace App\Http\Controllers\AdminDesa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Desa;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class DashboardDesaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_desa');
    }

    public function db()
    {
        return redirect()->route('pendudukdashboard.desa');
    }

    public function index($id)
    {
        $penduduk = DB::table('penduduk')
        ->select('penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->join('users','desa.id','=','users.desa_id')
        ->where('users.id', $id)->count();

        $positif = DB::table('positif') 
        ->select('positif.id')
        ->join('penduduk','positif.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->join('users','desa.id','=','users.desa_id')
        ->where('users.id', $id)->count();

        $meninggal = DB::table('positif') 
        ->select('positif.id')
        ->join('penduduk','positif.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->join('users','desa.id','=','users.desa_id')
        ->where('users.id', $id)
        ->where('kondisi', '2')->count();

        $sembuh = DB::table('positif') 
        ->select('positif.id')
        ->join('penduduk','positif.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->join('users','desa.id','=','users.desa_id')
        ->where('users.id', $id)
        ->where('kondisi', '0')->count();

        $vaksin = DB::table('vaksin')
        ->select('vaksin.id')
        ->join('penduduk','vaksin.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->join('users','desa.id','=','users.desa_id')
        ->where('users.id', $id)->count();

        $inap = DB::table('inap')
        ->select('inap.id')
        ->join('penduduk','inap.penduduk_id','=','penduduk.id')
        ->join('desa','penduduk.desa_id','=','desa.id')
        ->join('users','desa.id','=','users.desa_id')
        ->where('users.id', $id)
        ->where('inap.kondisi', 1)->count();

        $nama = DB::table('desa')
        ->select('desa.name as nama_desa')
        ->join('users','desa.id','=','users.desa_id')
        ->where('users.id', $id)->get();

        return view('adminDesa/dashboarddesa', compact('penduduk','positif','sembuh','vaksin','inap','meninggal','nama'));
    }
}
