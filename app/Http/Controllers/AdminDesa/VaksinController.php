<?php

namespace App\Http\Controllers\AdminDesa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Penduduk;
use App\Models\Vaksin;
use Illuminate\Support\Facades\DB;

class VaksinController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_desa');
    }

    public function index()
    {
        $vaksin = DB::table('vaksin')
        ->select('vaksin.id','vaksin.nama_vaksin')
        ->groupBy('vaksin.nama_vaksin')
        ->orderBy('vaksin.nama_vaksin','ASC')
        ->get();
 
        $data = DB::table('vaksin')
        ->select('vaksin.id','vaksin.nama_vaksin','vaksin.penduduk_id','penduduk.name','vaksin.tanggal','vaksin.jenis_vaksin')
        ->join('penduduk','vaksin.penduduk_id','=','penduduk.id')
        ->paginate(5);

        return view('adminDesa/datavaksin', compact('data','vaksin'));
    }

    public function searchvaksin($name_vaksin)
    {
        $vaksin = DB::table('vaksin')
        ->select('vaksin.id','vaksin.nama_vaksin')
        ->groupBy('vaksin.nama_vaksin')
        ->orderBy('vaksin.nama_vaksin','ASC')
        ->get();

        $data = DB::table('vaksin')
        ->select('vaksin.id','vaksin.nama_vaksin','vaksin.penduduk_id','penduduk.name','vaksin.tanggal','vaksin.jenis_vaksin')
        ->join('penduduk','vaksin.penduduk_id','=','penduduk.id')
        ->where('vaksin.nama_vaksin', $name_vaksin)
        ->paginate(5);

        return view('adminDesa/datavaksin', compact('data','vaksin'));
    }

    public function search(Request $request)
    {
        $vaksin = DB::table('vaksin')
        ->select('vaksin.id','vaksin.nama_vaksin')
        ->groupBy('vaksin.nama_vaksin')
        ->orderBy('vaksin.nama_vaksin','ASC')
        ->get();

        $data = DB::table('vaksin')
        ->select('vaksin.id','vaksin.nama_vaksin','vaksin.penduduk_id','penduduk.name','vaksin.tanggal','vaksin.jenis_vaksin')
        ->join('penduduk','vaksin.penduduk_id','=','penduduk.id')
        ->where('penduduk.name', 'like', "%{$request->search}%")
        ->orwhere('vaksin.penduduk_id', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('adminDesa/datavaksin', compact('data','vaksin'));
    }

    public function add()
    {
        return view('adminDesa/addvaksin');
    }

    public function cekname(Request $request)
    {
        
        $data = DB::table('penduduk')
            ->select('id','name')
            ->where('id', $request->id)
            ->get();
        
        $cek = DB::table('penduduk')
            ->select('id','name')
            ->where('id', $request->id)
            ->count();

        if($cek == 0){

            return redirect()->back()->with('error', ' Gagal !!! Data Pasien Tidak Ditemukan');

        }else{

            return view('adminDesa/ceknamevaksin',compact('data'));

        } 
    }

    public function store(Request $request)
    { 
            $request->validate([
                'tanggal' => 'required',
                'nama_vaksin' => 'required',
                'jenis_vaksin' => 'required',
            ],[
                'tanggal.required' =>'Error!!! Tanggal Tidak Boleh Kosong',
                'nama_vaksin.required' =>'Error!!! Nama Vaksin Ke Berapa Tidak Boleh Kosong',
                'jenis_vaksin.required' =>'Error!!! Nama Jenis Vaksin Tidak Boleh Kosong',
            ]);    

            $pus = new Vaksin();
            $pus->nama_vaksin = $request->nama_vaksin;
            $pus->penduduk_id = $request->penduduk_id;
            $pus->jenis_vaksin = $request->jenis_vaksin;
            $pus->tanggal = $request->tanggal;
            $pus->save();
        
        return redirect('vaksin')->with('success', 'Tambah Data Vaksin Berhasil');
    }

    public function hapus($id)
    { 
        try{

            Vaksin::find($id)->delete();

        }catch(\Exception $e){

            return redirect()->route('vaksin')->with('error', 'Data Tidak Bisa Dihapus!!!!');

        } 
        return redirect()->route('vaksin')->with('success', 'Data Berhasi Dihapus');  
    }
}
