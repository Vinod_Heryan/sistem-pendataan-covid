<?php

namespace App\Http\Controllers\AdminDesa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Sembuh;
use App\Models\Penduduk;
use Illuminate\Support\Facades\DB;

class SembuhController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_desa');
    }

    public function index()
    {
        $data = DB::table('sembuh')
        ->select('sembuh.id','sembuh.penduduk_id','penduduk.name','sembuh.tanggal')
        ->join('penduduk','sembuh.penduduk_id','=','penduduk.id')
        ->paginate(5);

        return view('adminDesa/datasembuh', compact('data'));
    }

    public function search(Request $request)
    {
        $data = DB::table('sembuh')
        ->select('sembuh.id','sembuh.penduduk_id','penduduk.name','sembuh.tanggal')
        ->join('penduduk','sembuh.penduduk_id','=','penduduk.id')
        ->where('penduduk.name', 'like', "%{$request->search}%")
        ->orwhere('penduduk.id', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('adminDesa/datasembuh', compact('data'));
    }

    public function hapus($id)
    { 
        try{

            Positif::find($id)->delete();

        }catch(\Exception $e){

            return redirect()->route('sembuh')->with('error', 'Data Tidak Bisa Dihapus!!!!');

        } 
        return redirect()->route('sembuh')->with('success', 'Data Berhasi Dihapus');  
    }
}
