<?php

namespace App\Http\Controllers\AdminDesa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Inap;
use App\Models\Penduduk;
use Illuminate\Support\Facades\DB;

class InapController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_desa');
    }

    public function index()
    {
        $no = DB::table('inap')
        ->select('inap.id','inap.kondisi')
        ->groupBy('inap.kondisi')
        ->get();

        $data = DB::table('inap')
        ->select('inap.id','inap.penduduk_id','penduduk.name','inap.tanggal_masuk','inap.kondisi','inap.tanggal_keluar','inap.tempat')
        ->join('penduduk','inap.penduduk_id','=','penduduk.id')
        ->orderBy('inap.kondisi','ASC')
        ->paginate(5);

        return view('adminDesa/datainap',compact('data','no'));
    }

    public function searchkondisi($id)
    {
        $no = DB::table('inap')
        ->select('inap.id','inap.kondisi')
        ->groupBy('inap.kondisi')
        ->get();

        $data = DB::table('inap')
        ->select('inap.id','inap.penduduk_id','penduduk.name','inap.tanggal_masuk','inap.kondisi','inap.tanggal_keluar','inap.tempat')
        ->join('penduduk','inap.penduduk_id','=','penduduk.id')
        ->where('inap.kondisi', $id)
        ->paginate(5);

        return view('adminDesa/datainap',compact('data','no'));
    }

    public function search(Request $request)
    {
        $no = DB::table('inap')
        ->select('inap.id','inap.kondisi')
        ->groupBy('inap.kondisi')
        ->get();

        $data = DB::table('inap')
        ->select('inap.id','inap.penduduk_id','penduduk.name','inap.tanggal_masuk','inap.kondisi','inap.tanggal_keluar','inap.tempat')
        ->join('penduduk','inap.penduduk_id','=','penduduk.id')
        ->where('penduduk.name', 'like', "%{$request->search}%")
        ->orwhere('penduduk.id', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('adminDesa/datainap',compact('data','no'));
    }

    public function add()
    {
        return view('adminDesa/addinap');
    }

    public function cekname(Request $request)
    {
        
        $data = DB::table('penduduk')
            ->select('id','name')
            ->where('id', $request->id)
            ->get();
        
        $cek = DB::table('penduduk')
            ->select('id','name')
            ->where('id', $request->id)
            ->count();

        if($cek == 0){

            return redirect()->back()->with('error', ' Gagal !!! Data Pasien Tidak Ditemukan');

        }else{

            return view('adminDesa/ceknameinap',compact('data'));

        } 
    }

    public function store(Request $request)
    { 
            $request->validate([
                'tanggal_masuk' => 'required',
                'tempat' => 'required',
            ],[
                'tanggal_masuk.required' =>'Error!!! Tanggal Tidak Boleh Kosong',
                'tempat.required' =>'Error!!! Tempat Inap Tidak Boleh Kosong',
            ]);    

            $pus = new Inap();
            $pus->penduduk_id = $request->penduduk_id;
            $pus->tanggal_masuk = $request->tanggal_masuk;
            $pus->kondisi = '1';
            $pus->tempat = $request->tempat;
            $pus->save();
        
            return redirect('inap')->with('success', 'Tambah Data Rawat Inap Berhasil');
    }

    public function update($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        Inap::where('id', $id)->update([
            'kondisi' => '0',
            'tanggal_keluar' => date('Y-m-d'),
        ]);

        return redirect()->route('inap')->with('success', 'Data Pasien Berhasi Diupdate'); 
    }

    public function hapus($id)
    { 
        try{

            Vaksin::find($id)->delete();

        }catch(\Exception $e){

            return redirect()->route('inap')->with('error', 'Data Tidak Bisa Dihapus!!!!');

        } 
        return redirect()->route('inap')->with('success', 'Data Berhasi Dihapus');  
    }
}
