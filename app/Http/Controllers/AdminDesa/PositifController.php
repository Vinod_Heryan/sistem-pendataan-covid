<?php

namespace App\Http\Controllers\AdminDesa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Positif;
use App\Models\Sembuh;
use App\Models\Penduduk;
use Illuminate\Support\Facades\DB;

class PositifController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_desa');
    }

    public function index()
    {
        $no = DB::table('positif')
        ->select('positif.id','positif.kondisi')
        ->groupBy('positif.kondisi')
        ->orderBy('positif.kondisi','ASC')
        ->get();

        $data = DB::table('positif')
        ->select('positif.id','positif.penduduk_id','penduduk.name','positif.tanggal','positif.kondisi',
        'positif.tanggal_update')
        ->join('penduduk','positif.penduduk_id','=','penduduk.id')
        ->orderBy('positif.kondisi','ASC')
        ->paginate(5);

        return view('adminDesa/datapositif',compact('data','no'));
    }

    public function searchkondisi($kondisi)
    {
        $no = DB::table('positif')
        ->select('positif.id','positif.kondisi')
        ->groupBy('positif.kondisi')
        ->orderBy('positif.kondisi','ASC')
        ->get();

        $data = DB::table('positif')
        ->select('positif.id','positif.penduduk_id','penduduk.name','positif.tanggal','positif.kondisi',
        'positif.tanggal_update')
        ->join('penduduk','positif.penduduk_id','=','penduduk.id')
        ->where('positif.kondisi', $kondisi)
        ->paginate(5);

        return view('adminDesa/datapositif', compact('data','no'));
    }

    public function search(Request $request)
    {
        $no = DB::table('positif')
        ->select('positif.id','positif.kondisi')
        ->groupBy('positif.kondisi')
        ->orderBy('positif.kondisi','ASC')
        ->get();

        $data = DB::table('positif')
        ->select('positif.id','positif.penduduk_id','penduduk.name','positif.tanggal','positif.kondisi',
        'positif.tanggal_update')
        ->join('penduduk','positif.penduduk_id','=','penduduk.id')
        ->where('penduduk.name', 'like', "%{$request->search}%")
        ->orwhere('penduduk.id', 'like', "%{$request->search}%")
        ->paginate(5);

        return view('adminDesa/datapositif',compact('data','no'));
    }

    public function add()
    {
        return view('adminDesa/addpositif');
    }

    public function cekname(Request $request)
    {
        
        $data = DB::table('penduduk')
            ->select('id','name')
            ->where('id', $request->id)
            ->get();
        
        $cek = DB::table('penduduk')
            ->select('id','name')
            ->where('id', $request->id)
            ->count();

        if($cek == 0){

            return redirect()->back()->with('error', ' Gagal !!! Data Pasien Tidak Ditemukan');

        }else{

            return view('adminDesa/cekname',compact('data'));

        } 
    }

    public function store(Request $request)
    { 
            $request->validate([
                'tanggal' => 'required',
            ],[
                'tanggal.required' =>'Error!!! Tanggal Tidak Boleh Kosong',
            ]);    

            $pus = new Positif();
            $pus->penduduk_id = $request->penduduk_id;
            $pus->tanggal = $request->tanggal;
            $pus->kondisi = '1';
            $pus->save();
        
            return redirect('positif')->with('success', 'Tambah Data Positif Berhasil');
    }

    public function updateSembuh($id)
    {
        date_default_timezone_set('Asia/Jakarta');

        Positif::where('id', $id)->update([
            'kondisi'=>'0',
            'tanggal_update'=> date('Y-m-d'),
        ]);

        return redirect()->route('positif')->with('success', 'Data Pasien Berhasi Diupdate'); 
    }

    public function updateMeninggal($id)
    {
        date_default_timezone_set('Asia/Jakarta');

        Positif::where('id', $id)->update([
            'kondisi'=>'2',
            'tanggal_update'=> date('Y-m-d'),
        ]);

        return redirect()->route('positif')->with('success', 'Data Pasien Berhasi Diupdate'); 
    }

    public function hapus($id)
    { 
        try{

            Positif::find($id)->delete();

        }catch(\Exception $e){

            return redirect()->route('positif')->with('error', 'Data Tidak Bisa Dihapus!!!!');

        } 
        return redirect()->route('positif')->with('success', 'Data Berhasi Dihapus');  
    }
}
