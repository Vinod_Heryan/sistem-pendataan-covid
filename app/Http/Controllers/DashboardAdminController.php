<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inap;
use App\Models\Desa;
use App\Models\Positif;
use App\Models\Sembuh;
use App\Models\Vaksin;
use App\Models\Penduduk;


class DashboardAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_admin');
    }

    public function index()
    {
        $penduduk = Penduduk::all()->count();

        $positif = Positif::all()->count();

        $sembuh = positif::where('kondisi','0')->count();
 
        $vaksin = Vaksin::all()->count();

        $inap = Inap::all()->where('kondisi', 1)->count();

        $meninggal = positif::where('kondisi','2')->count();

        return view('adminPusat/dashboardadmin', compact('penduduk','positif','sembuh','vaksin','inap','meninggal'));
    }
}
