<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inap extends Model
{
    use HasFactory;

    protected $table = 'inap';

    protected $fillable = [
        'penduduk_id', 'tanggal_masuk','kondisi','tanggal_keluar','tempat'
    ];
}
