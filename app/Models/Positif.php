<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Positif extends Model
{
    use HasFactory;

    protected $table = 'positif';

    protected $fillable = [
        'penduduk_id', 'tanggal','kondisi',
    ];
}
