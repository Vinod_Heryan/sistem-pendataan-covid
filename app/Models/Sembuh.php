<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sembuh extends Model
{
    use HasFactory;

    protected $table = 'sembuh';

    protected $fillable = [
        'penduduk_id', 'tanggal',
    ];
}
